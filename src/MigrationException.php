<?php declare(strict_types=1);

namespace ThibaudDauce\Migrations;

use RuntimeException;

class MigrationException extends RuntimeException
{

}