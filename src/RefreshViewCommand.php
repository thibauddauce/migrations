<?php

namespace ThibaudDauce\Migrations;

use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Illuminate\Database\Migrations\Migrator;

class RefreshViewCommand extends Command
{
    protected $signature = 'migrate:refresh-view {migration-file}';

    public function handle()
    {
        $file = $this->argument('migration-file');

        require_once($file);

        $migration = $this->resolve($this->getMigrationName($file));

        if (!($migration instanceof ViewMigration)) {
            $this->error("Cannot refresh a table migration (only view).");
            return 1;
        }

        $this->info("Rolling back view…");
        $migration->down();

        $this->info("Creating view…");
        $migration->up();

        $this->info("Done.");
    }

    /**
     * Copied from Migrator.php
     */
    public function getMigrationName($path)
    {
        return base_path(str_replace('.php', '', basename($path)));
    }

    /**
     * Copied from Migrator.php
     */
    protected function resolve($file)
    {
        $class = Str::studly(implode('_', array_slice(explode('_', $file), 4)));

        return new $class;
    }
}
