<?php declare(strict_types=1);

namespace ThibaudDauce\Migrations;

class MissingTableDefinition extends MigrationException
{
    public function __construct(Migration $migration)
    {
        $modelMethod = get_class($migration->getModel()) . "@{$migration->getVia()}";
        $migrationMethod = get_class($migration) . "@{$migration->getVia()}";

        parent::__construct(
            "The table definition should exists in `$modelMethod` or in `$migrationMethod`. Neither method is defined."
        );
    }
}