<?php

namespace ThibaudDauce\Migrations;

use Exception;

class UnsetModel extends Exception
{
    public function __construct()
    {
        parent::__construct("You should set the model with `setModel` before using any new Blueprint method related to models.");
    }
}
