<?php declare(strict_types=1);

namespace ThibaudDauce\Migrations;

use Illuminate\Database\Migrations\Migration as LaravelMigration;
use Illuminate\Support\Facades\DB;

class ViewMigration extends LaravelMigration
{
    use InteractsWithModel;

    protected $via = 'schema';

    public function up()
    {
        $query = $this->getViewQuery();

        if (! is_string($query)) {
            $query = $query->toSql();
        }

        DB::statement("CREATE VIEW {$this->getModel()->getTable()} AS {$query}");
    }

    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS {$this->getModel()->getTable()}");
    }

    private function getViewQuery()
    {
        if (method_exists($this, $this->via)) {
            return $this->{$this->via}();
        }

        if (method_exists($this->getModel(), $this->via)) {
            return $this->getModel()->{$this->via}();
        }

        throw new MissingViewDefinition($this);
    }

    public function getVia()
    {
        return $this->via;
    }
}
