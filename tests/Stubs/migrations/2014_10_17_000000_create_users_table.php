<?php

use Illuminate\Database\Schema\Blueprint;
use ThibaudDauce\Migrations\Migration;
use ThibaudDauce\Migrations\Stubs\User;

class CreateUsersTable extends Migration
{
    protected $model = User::class;

    public function schema(Blueprint $table)
    {
        $table->increments('id');
        $table->string('email')->unique();
        $table->string('password');
        $table->timestamps();
    }
}
