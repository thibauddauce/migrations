<?php

use ThibaudDauce\Migrations\Migration;
use ThibaudDauce\Migrations\Stubs\Post;

class CreatePostsTable extends Migration
{
    protected $model = Post::class;
}
