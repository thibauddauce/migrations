<?php

use ThibaudDauce\Migrations\Migration;
use ThibaudDauce\Migrations\Stubs\StringIDModel;

class CreateStringIDModelsTable extends Migration
{
    protected $model = StringIDModel::class;
}
