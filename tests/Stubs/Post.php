<?php

namespace ThibaudDauce\Migrations\Stubs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;

class Post extends Model
{
    protected $guarded = [];
    protected $attributes = [
        'active' => false,
    ];

    public function schema(Blueprint $table)
    {
        $table->increments('id');
        $table->boolean('active');
        $table->string('title')->unique();
        $table->text('body');

        $table->relation('stringIDModel')->nullable();
        $table->timestamps();
    }

    public function stringIDModel()
    {
        return $this->belongsTo(StringIDModel::class, 'string_id_model_id');
    }
}
