<?php

namespace ThibaudDauce\Migrations\Stubs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;

class Comment extends Model
{
    protected $guarded = [];

    public function schema(Blueprint $table)
    {
        $table->increments('id');
        $table->text('body');
        $table->relation('post');
        $table->timestamps();
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
