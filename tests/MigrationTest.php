<?php

namespace ThibaudDauce\Migrations;

use Orchestra\Testbench\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use ThibaudDauce\Migrations\Stubs\ActivePost;
use ThibaudDauce\Migrations\Stubs\Post;
use ThibaudDauce\Migrations\Stubs\Search;
use ThibaudDauce\Migrations\Stubs\Comment;
use ThibaudDauce\Migrations\Stubs\StringIDModel;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;
use ThibaudDauce\Migrations\Stubs\User;

class MigrationTest extends TestCase
{
    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('database.default', 'testbench');
        $app['config']->set('database.connections.testbench', [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]);
    }

    protected function getPackageProviders($app)
    {
        return [
            ServiceProvider::class,
            TestServiceProvider::class,
        ];
    }

    public function setUp(): void
    {
        parent::setUp();

        $this->artisan('migrate', ['--database' => 'testbench']);
        DB::statement('PRAGMA foreign_keys = ON;');
    }

    public function tearDown(): void
    {
        Search::setDefaultSchema();
        $this->artisan('migrate:refresh-view', ['migration-file' => 'tests/Stubs/migrations/2014_10_15_000000_create_searches_view.php']);
     
        parent::tearDown();
    }

    /** @test */
    function it can migrate two related models()
    {
        $post = Post::create([
            'title' => 'My Title',
            'body' => 'My post body.',
        ]);

        $comment = Comment::create([
            'post_id' => $post->id,
            'body' => 'My comment body.',
        ]);

        $this->assertEquals('INTEGER', $this->getColumn('comments', 'post_id')->type);
        $this->assertTrue($comment->post->is($post));
    }

    /** @test */
    function it sets the correct foreign key relation in database()
    {
        try {
            Comment::create([
                'post_id' => 1,
                'body' => 'My comment without related Post.',
            ]);

            $this->fail("Successfuly created a comment without related Post.");
        } catch (QueryException $e) {
            $this->assertStringContainsString("Integrity constraint violation", $e->getMessage());
        }
    }

    /** @test */
    function it creates a string column for string based models IDs()
    {
        $stringIDModel = StringIDModel::create([
            'id' => 'some-string',
        ]);

        $post = Post::create([
            'title' => 'My Title',
            'body' => 'My post body.',
            'string_id_model_id' => 'some-string',
        ]);

        $this->assertEquals('varchar', $this->getColumn('posts', 'string_id_model_id')->type);
        $this->assertTrue($post->stringIDModel->is($stringIDModel));
    }

    /** @test */
    function it creates a view()
    {
        $post = Post::create([
            'title' => 'My Title',
            'body' => 'My post body.',
        ]);

        $comment = Comment::create([
            'post_id' => $post->id,
            'body' => 'My comment body.',
        ]);

        $searches = Search::all();

        $this->assertTrue($searches->contains(function ($search) use ($post) {
            return $search->term === 'My Title'
               and $search->searchable_id === $post->id
               and $search->searchable_type === Post::class;
        }), "Couldn't find 'My Title' in the view.");
        $this->assertTrue($searches->contains(function ($search) use ($post) {
            return $search->term === 'My post body.'
               and $search->searchable_id === $post->id
               and $search->searchable_type === Post::class;
        }), "Couldn't find 'My Title' in the view.");
        $this->assertTrue($searches->contains(function ($search) use ($comment) {
            return $search->term === 'My comment body.'
               and $search->searchable_id === $comment->id
               and $search->searchable_type === Comment::class;
        }), "Couldn't find 'My Title' in the view.");
    }

    /** @test */
    function it refreshes a view()
    {
        $post = Post::create([
            'title' => 'My Title',
            'body' => 'My post body.',
        ]);

        $comment = Comment::create([
            'post_id' => $post->id,
            'body' => 'My comment body.',
        ]);

        $this->assertEquals(3, Search::count());
    
        Search::setOnlyTitleSchema();

        $this->artisan('migrate:refresh-view', ['migration-file' => 'tests/Stubs/migrations/2014_10_15_000000_create_searches_view.php']);

        $this->assertEquals(1, Search::count());
    }

    /** @test */
    function cannot refresh a table migration()
    {
        $this->artisan('migrate:refresh-view', [
            'migration-file' => 'tests/Stubs/migrations/2014_10_12_000000_create_posts_table.php'
        ])->assertExitCode(1);
    }

    /** @test */
    function it creates a view with the definition inside the migration()
    {
        Post::create([
            'title' => 'one post',
            'body' => 'one content',
            'active' => true,
        ]);
        Post::create([
            'title' => 'one draft',
            'body' => 'one drafted content',
            'active' => false,
        ]);

        $this->assertCount(1, ActivePost::all());
    }

    /** @test */
    function it creates a table with the definition inside the migration()
    {
        User::create([
            'email' => 'thibaud@example.org',
            'password' => bcrypt('secret'),
        ]);

        $this->assertCount(1, User::all());
    }

    protected function getColumn($table, $name)
    {
        return collect(DB::select("PRAGMA table_info({$table});"))
            ->where('name', $name)
            ->first();
    }
}

class TestServiceProvider extends LaravelServiceProvider
{
    public function boot()
    {
        $this->loadMigrationsFrom(realpath(__DIR__ . '/Stubs/migrations'));
    }
}
